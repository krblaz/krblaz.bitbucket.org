/*global $*/
/*global btoa*/
/*global google*/

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var username = "ois.seminar";
var password = "ois4fri";
var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImJsYXp6eC5iYWNAZ21haWwuY29tIiwicm9sZSI6IlVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9zaWQiOiI1MTEwIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy92ZXJzaW9uIjoiMjAwIiwiaHR0cDovL2V4YW1wbGUub3JnL2NsYWltcy9saW1pdCI6Ijk5OTk5OTk5OSIsImh0dHA6Ly9leGFtcGxlLm9yZy9jbGFpbXMvbWVtYmVyc2hpcCI6IlByZW1pdW0iLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL2xhbmd1YWdlIjoiZW4tZ2IiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL2V4cGlyYXRpb24iOiIyMDk5LTEyLTMxIiwiaHR0cDovL2V4YW1wbGUub3JnL2NsYWltcy9tZW1iZXJzaGlwc3RhcnQiOiIyMDE5LTA1LTE1IiwiaXNzIjoiaHR0cHM6Ly9zYW5kYm94LWF1dGhzZXJ2aWNlLnByaWFpZC5jaCIsImF1ZCI6Imh0dHBzOi8vaGVhbHRoc2VydmljZS5wcmlhaWQuY2giLCJleHAiOjE1NTgxOTQ4MTAsIm5iZiI6MTU1ODE4NzYxMH0.DNKFxUetQ40_9Hi6n34F6dgQSwDae_tvd50lSmHpC4U";

google.charts.load('current', { 'packages': ['corechart'] });

var pacienti = ["01d69de8-60b9-43da-8908-b1c42d26501f", "dfdbd362-1802-459f-9bf6-e2977404aad4", "b428f657-d5fa-4038-bd08-2901037644bb"];
var simptomi;
var spol;
var letoRojstva;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
var authorization = "Basic " + btoa(username + ":" + password);

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function pretvoriDatum(datumOriginalni) {
  var datum = new Date(datumOriginalni);
  var mesec;
  switch (datum.getMonth()) {
    case 1:
      mesec = "Januar";
      break;
    case 2:
      mesec = "Februar";
      break;
    case 3:
      mesec = "Marec";
      break;
    case 4:
      mesec = "April";
      break;
    case 5:
      mesec = "Maj";
      break;
    case 6:
      mesec = "Junij";
      break;
    case 7:
      mesec = "Julij";
      break;
    case 8:
      mesec = "Avgust";
      break;
    case 9:
      mesec = "September";
      break;
    case 10:
      mesec = "Oktober";
      break;
    case 11:
      mesec = "November";
      break;
    case 12:
      mesec = "December";
  }
  letoRojstva = datum.getFullYear();
  return datum.getDate() + ". " + mesec + " " + datum.getFullYear();
}

function generirajPodatke(stPacienta) {
  var ehrId;
  $.ajax({
    url: baseUrl + "/ehr",
    async: false,
    type: "POST",
    headers: { "Authorization": authorization },
    success: function(data) {
      ehrId = data.ehrId;
      $.getJSON("knjiznice/json/podatki.json", function(json) {
        json.osnovniPodatki[stPacienta - 1].partyAdditionalInfo[0].value = data.ehrId;
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: "POST",
          headers: { "Authorization": authorization },
          contentType: 'application/json',
          data: JSON.stringify(json.osnovniPodatki[stPacienta - 1]),
          success: function() {
            var queryParams = {
              "ehrId": ehrId,
              templateId: 'Vital Signs',
              format: 'FLAT'
            };
            $.ajax({
              url: baseUrl + "/composition?" + $.param(queryParams),
              headers: { "Authorization": authorization },
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(json.vitalniZnaki[stPacienta - 1])
            });
          }
        });
      });
    }
  });
  return ehrId;
}

function naloziDemografskePodatke(ehrId) {
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    headers: { "Authorization": authorization },
    type: "GET",
    success: function(party) {
      $("#imeInPriimek").html(party.party.firstNames + " " + party.party.lastNames);
      $("#spol").html(party.party.gender == "FEMALE" ? "Ženska" : "Moški");
      spol = party.party.gender;
      $("#datumRojstva").html(pretvoriDatum(party.party.dateOfBirth));
    }
  });
}

function naloziOsnovneVitalneZnake(ehrId) {
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    headers: { "Authorization": authorization },
    type: "GET",
    success: function(height) {
      var visina = Math.round(height[0].height) / 100;
      $("#visina").html(visina + " m");
      $("#modalVisina").html(visina + " m");
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/weight",
        headers: { "Authorization": authorization },
        type: "GET",
        success: function(weight) {
          $("#teza").html(weight[0].weight + " kg");
          $("#modalTeza").html(weight[0].weight + " kg");
          var BMI = parseFloat(Math.round(weight[0].weight / (height[0].height * height[0].height) * 1000000) / 100).toFixed(2);
          $("#bmi").html(BMI);
          $("#modalBMI").html(BMI);
          $("#toggleModalVitalni").click(function() {
            var povprecnaVisina = spol == "FEMALE" ? 1.674 : 1.803;
            var povprecnaBMI = spol == "FEMALE" ? 26.3 : 27.5;
            var izracunanaVisina = Math.round((visina / povprecnaVisina - 1) * 10000) / 100;
            var izracunanaTeza = Math.round((weight[0].weight / 71.2 - 1) * 10000) / 100;
            var izracunanBMI = Math.round((BMI / povprecnaBMI - 1) * 10000) / 100;
            $("#procentiVisina").html(Math.abs(izracunanaVisina) + "% " + (izracunanaVisina <= 0 ? "pod" : "nad"));
            $("#procentiTeza").html(Math.abs(izracunanaTeza) + "% " + (izracunanaTeza <= 0 ? "pod" : "nad"));
            $("#procentiBMI").html(Math.abs(izracunanBMI) + "% " + (izracunanBMI <= 0 ? "pod" : "nad"));
          });
        }
      });
    }
  });
}

function graf(ehrId, getType, dataType, dataName, format, enota, max, min) {
  var podatki = [];
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + getType, //TLE
    type: "GET",
    headers: { "Authorization": authorization },
    success: function(data) {
      for (var i in data) {
        podatki[i] = [new Date(data[i].time), data[i][dataType]]; //TLE
      }
      $("#" + dataType + "Zadnji").html(Math.round(data[0][dataType] * 10) / 10 + enota);
      google.charts.setOnLoadCallback(drawChart);
    }
  });

  function drawChart() {
    var podatkiZaGraf = new google.visualization.DataTable();
    podatkiZaGraf.addColumn("date", "Datum");
    podatkiZaGraf.addColumn("number", dataName); //TLE
    podatkiZaGraf.addRows(podatki);

    var options = {
      legend: { position: "none" },
      fontName: "Segoe UI",
      vAxis: {
        format: format, //TLE
        gridlines: {
          color: "#dee2e6",
          count: 4
        },
        minorGridlines: {
          count: 0
        },
        textPosition: "in",
        maxValue: max, //TLE
        minValue: min, //TLE
      },
      hAxis: {
        format: "d/M/YY",
        gridlines: {
          color: "none",
          count: 4
        },
        minorGridlines: {
          count: 0
        },
        textPosition: "in"
      },
      chartArea: {
        left: 0,
        top: 0,
        width: "100%",
        height: "100%"
      },
      colors: ["#007bff"]
    };
    var chart = new google.visualization.LineChart(document.getElementById(dataType)); //TLE
    chart.draw(podatkiZaGraf, options);
  }
}


function naloziPodatke(ehrId) {
  naloziDemografskePodatke(ehrId);
  naloziOsnovneVitalneZnake(ehrId);
  graf(ehrId, "body_temperature", "temperature", "Telesna temperatura", "## °C", " °C", 43, 34.5);
  graf(ehrId, "pulse", "pulse", "Srčni utrip", "# u/m", " u/m", 85, 50);
  graf(ehrId, "blood_pressure", "diastolic", "Diastolični tlak", "# mmHg", " mmHg", 120, 40);
  graf(ehrId, "blood_pressure", "systolic", "Sistolični tlak", "# mmHg", " mmHg", 190, 70);
  simptomi = [];
  $(".oksign").remove();
}

function zgenerirajVsePodatke() {
  $("#zgeneriraniPacineti").html("");
  for (var i = 1; i <= 3; i++) {
    pacienti[i - 1] = generirajPodatke(i);
    $("#zgeneriraniPacineti").append("<li>" + pacienti[i - 1] + "</li>");
  }
}

$(document).ready(function() {
  $("#generiranjePodatkov").click(function() {
    zgenerirajVsePodatke();
  });
  //naloži podatke o pacientu
  naloziPodatke(pacienti[0]);
  $(".zamenjajUporabnikaDropdownItem").click(function() {
    if ($(this).index() < 3) {
      naloziPodatke(pacienti[$(this).index()]);
      $(".profilnaSlika").attr("src", "p" + $(this).index() + ".jpg");
    }
  });
  $("#vnosEhrOK").click(function() {
    naloziPodatke($("#vnosEhrVrednost").val());
    $(".profilnaSlika").attr("src", "neznanUporabnik.jpg");
    $("#vnosEhrVrednost").val("");
  });
  $("#vnosEhrClose").click(function() {
    $("#vnosEhrVrednost").val("");
  });

  $("#vnosTokenOK").click(function() {
    token = $("#vnosToken").val();
    naloziSimptome();
    $("#vnosToken").val("");
  });

  $("#vnosTokenClose").click(function() {
    $("#vnosToken").val("");
  });

  $(".poisciDiagnozo").click(function() {
    poisciDiagnozo();
  });

  naloziSimptome();

});

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


function naloziSimptome() {
  $.ajax({
    url: "https://sandbox-healthservice.priaid.ch/symptoms?token=" + token + "&format=json&language=en-gb",
    type: "GET",
    headers: {},
    success: function(data) {
      for (var i in data) {
        if (data[i].Name.length <= 22) {
          $(".simptomiList").append("<div class='simptomiListItem' simptomId='" + data[i].ID + "'" + (i == 0 ? " style='border-top: none'" : "") + ">" + data[i].Name + "</div>");
        }
      }
      $(".simptomiListItem").click(function() {
        var simptomId = $(this).attr("simptomId");
        if (simptomi.includes(simptomId)) {
          simptomi.splice(simptomi.indexOf(simptomId), 1);
          $(this).children("span").remove();
        }
        else {
          simptomi.push(simptomId);
          $(this).append("<span class='glyphicon glyphicon-ok oksign'></span>");
        }
      });
    }
  });
}

function poisciDiagnozo() {
  if (simptomi.length == 0) {
    $("#najdeneDiagnoze").html("Niste vnesli dovolj podatkov");
  }
  else {
    $.ajax({
      url: "https://sandbox-healthservice.priaid.ch/diagnosis?symptoms=[" + simptomi + "]&gender=" + spol + "&year_of_birth=" + letoRojstva + "&token=" + token + "&format=json&language=en-gb",
      type: "GET",
      success: function(data) {
        if (data.length == 0) {
          $("#najdeneDiagnoze").html("Ni najdene nobena diagnoza");
        }
        else {
          $("#najdeneDiagnoze").html("Možne diagnoze:");
          for (var i = 0; i < data.length && i < 5; i++) {
            $("#najdeneDiagnoze").append("<li>" + data[i].Issue.Name + " (" + data[i].Issue.Accuracy + " %)</li>");
          }
        }
        simptomi = [];
        $(".oksign").remove();
      }
    });
  }
}
