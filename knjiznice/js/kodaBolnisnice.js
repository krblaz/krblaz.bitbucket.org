/*global L*/
/*global $*/
/*global distance*/

var mymap;
var geoJSON;

$(document).ready(function() {
    mymap = L.map('mapid').setView([46.0655871, 14.5197752], 12);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        id: 'mapbox.streets'
    }).addTo(mymap);

    var ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-blue.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
    L.Marker.prototype.options.icon = ikona;
    
    pridobiPodatke();
    mymap.on("click", mapClick);
});

function pridobiPodatke() {
    $.ajax({
        url: "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json",
        type: "GET",
        success: function(data) {
            geoJSON = L.geoJSON(data,{style: {color: "blue"}, onEachFeature: onEachFeature}).addTo(mymap);
        }
    });
}

function mapClick(click) {
    var najblizjaRazdalja;
    var najblizjiIndeks;
    geoJSON.eachLayer(function(layer){
        if(layer.feature.geometry.type=="Polygon"){
            layer.setStyle({color: "blue"});
            var dolzina = distance(click.latlng.lat,click.latlng.lng,layer.feature.geometry.coordinates[0][0][1],layer.feature.geometry.coordinates[0][0][0],"K");
            najblizjaRazdalja = najblizjaRazdalja == undefined ? dolzina : najblizjaRazdalja;
            if(dolzina < najblizjaRazdalja){
                najblizjaRazdalja = dolzina;
                najblizjiIndeks = layer.feature.id;
            }
        }
    });
    geoJSON.eachLayer(function(layer){
        if(layer.feature.id == najblizjiIndeks){
            layer.setStyle({color: "green"});
        }
    });
}

function onEachFeature(feature, layer){
    if(feature.properties.name != undefined){
        layer.bindPopup(feature.properties.name + ((feature.properties["addr:street"] == undefined || feature.properties["addr:housenumber"] == undefined) ? "" : ("<br>" + feature.properties["addr:street"] + " " + feature.properties["addr:housenumber"])));
    }
}